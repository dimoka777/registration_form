from django.contrib.auth.models import AbstractUser
from django.db import models


class CustomerUser(AbstractUser):
    name = models.CharField(max_length=50)
    phone = models.CharField(max_length=20)
    user_avatar = models.ImageField()

    def __str__(self):
        return '{}{}{}'.format(self.name, self.phone, self.avatar)


class Driver(AbstractUser):
    driver_name = models.CharField(max_length=50)
    driver_phone = models.CharField(max_length=50)
    car_model = models.CharField(max_length=30, verbose_name='Модель авто')
    seat_amount = models.IntegerField(choices=list(zip(range(1, 10), range(1, 10))), unique=True)
    car_plate = models.CharField(max_length=20)
    driver_avatar = models.ImageField()

    def __str__(self):
        return '{}{}{}{}{}{}'.format(self.driver_name, self.driver_phone, self.car_model, self.seat_amount,
                                     self.car_plate, self.driver_avatar)


